import ForgeUI, {
  Text,
  TextField,
  Form,
  Button,
  ButtonSet,
  Fragment,
  useState,
  useAction
} from "@forge/ui";
import { FieldType } from "./types";

interface Config {
  fields: FieldType[];
  title: string;
}

interface ConfigViewProps {
  setConfig: (config: Config) => Promise<void>;
  existingConfig: Config;
}

interface AddAction {
  type: "add";
}

interface RemoveAction {
  type: "remove";
}

type FieldsNumAction = AddAction | RemoveAction;

const ConfigView = ({ setConfig, existingConfig }: ConfigViewProps) => {
  const [existingFieldState, setExistingFieldState] = useState<
    Record<number, FieldType>
  >(Object.assign({}, existingConfig.fields));
  const [fieldsNum, updateFieldsNum] = useAction<number[], FieldsNumAction>(
    (prev, action) => {
      switch (action.type) {
        case "add":
          console.log("add before", prev);
          if (prev.length === 0) {
            return [0];
          }
          return [...prev, prev[prev.length - 1] + 1];
        case "remove":
          return prev.slice(0, -1);
        default:
          return prev;
      }
    },
    [...Array(existingConfig.fields.length).keys()]
  );
  return (
    <Fragment>
      <Text content="Thanks for using Simple Sign-up!" />

      <Form
        onSubmit={async data => {
          const fields: FieldType[] = Array.from(
            { length: fieldsNum.length },
            (_, i) => {
              return {
                type: "TextField",
                label: data[`${i}-field`]
              };
            }
          );
          await setConfig({ fields, title: data.title });
        }}
        submitButtonText="Start signing people up! 🎉"
      >
        <TextField
          name="title"
          label="What are people signing up for? (optional)"
          defaultValue={existingConfig.title}
        />
        <Text
          content={`

        **Is there any extra information you would like to get with your sign-ups (in addition to name)?**`}
        />
        {fieldsNum.length > 0 &&
          fieldsNum.map((fieldNum: number, i: number) => (
            <TextField
              label={""}
              name={`${fieldNum}-field`}
              defaultValue={
                existingFieldState[fieldNum]
                  ? existingFieldState[fieldNum].label
                  : ""
              }
            />
          ))}
        <ButtonSet>
          <Button
            text="Add extra information field"
            onClick={() => {
              updateFieldsNum({ type: "add" });
            }}
          />
          <Button
            text="Remove last field"
            onClick={() => {
              updateFieldsNum({ type: "remove" });
              const lastFieldsNum = fieldsNum[fieldsNum.length - 1];
              const matchingExistingField = existingFieldState[lastFieldsNum];
              if (matchingExistingField) {
                const tempExistingFieldState = existingFieldState;
                delete tempExistingFieldState[lastFieldsNum];
                setExistingFieldState(tempExistingFieldState);
              }
            }}
          />
        </ButtonSet>
      </Form>
    </Fragment>
  );
};

export default ConfigView;
