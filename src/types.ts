export interface EntryData {
  name: string;
  accountId: string;
  [key: string]: any;
}

export interface Metadata {
  hasPublished: boolean;
  fields: FieldType[];
  title: string;
}

export interface TextFieldType {
  type: "TextField";
  label: string;
}

export type FieldType = TextFieldType;
