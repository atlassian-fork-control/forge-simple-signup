# Forge Simple Sign-up Macro

This Forge app adds an easy-to-use sign-up form to your Confluence Cloud page or blog.

## Configuration view

![Simple Sign-up Config](images/simple-signup-config.png)

## Sign-up view

![Simple Sign-up](images/simple-signup.png)

## Code

The app code can be found in [src/index.tsx](./src/index.tsx).

Each instance of the app will first display the configuration view, then after config has been set, it will display the sign-up form to all users.
The sign-up form will either be a single button to sign-up, or if additional extra information field config has been set, it will display form fields to fill in before signing up.

## Installing this app

You will need Node.js and the Forge CLI to install this app. You can install the Forge CLI by running `npm install -g @forge/cli`.

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository.
1. Run `forge register` to register a new copy of this app to your developer account.
1. Run `npm install` to install your dependencies.
1. Run `forge deploy` to deploy the app into the default environment.
1. Run `forge install` and follow the prompts to install the app into your Confluence site.

The **Simple Sign-up** macro should now appear in your macro browser.

At this point, you now have your own copy of a Forge app.
